# TODO

Must be done before version 1.0.0 release.

- [X] ~~Silent task~~
- [ ] Task directory
- [ ] Runtime checks
  - [ ] File time comparison
- [ ] Stepping through task-file
- [ ] Ignore errors
- [ ] Directly call task from another task
- [ ] Environmental variables
  - [ ] Local and global
- [ ] Includes and templates?
- [ ] Task logging prefix
- [ ] Convert over to [yaml-rust](https://github.com/chyh1990/yaml-rust)?
- [X] ~~Convert to custom template engine~~
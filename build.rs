use std::env;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::Path;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let path = Path::new(&out_dir).join("version");
    let mut f = BufWriter::new(File::create(&path).unwrap());

    writeln!(f, "host: {}", env::var("HOST").unwrap()).unwrap();
    writeln!(f, "target: {}", env::var("TARGET").unwrap()).unwrap();

    writeln!(f, "").unwrap();

    writeln!(f, "profile: {}", env::var("PROFILE").unwrap()).unwrap();
    writeln!(f, "version: {}", env::var("CARGO_PKG_VERSION").unwrap()).unwrap();
}

//! Logging wrapper around `stdout` and `write`.
//!
//! Uses `colored` internally.

use std::{
    fmt::Display,
    io::{self, Result, Write},
};

use colored::*;

/// Logging, just a holder for `stdout`.
pub struct Log {
    out: io::Stdout,
}

impl Log {
    /// Create new Log(ger).
    pub fn new() -> Self {
        Self::default()
    }

    /// Error log function.
    pub fn error<M>(&mut self, msg: M) -> Result<()>
    where
        M: AsRef<str> + Display,
    {
        writeln!(&mut self.out, "{} {}", "e".red(), msg)
    }

    /// Info log function.
    pub fn info<M>(&mut self, msg: M) -> Result<()>
    where
        M: AsRef<str> + Display,
    {
        writeln!(&mut self.out, "{} {}", "i".blue(), msg)
    }

    /// Error log function, with task display.
    pub fn task_error<T, M>(&mut self, task: T, msg: M) -> Result<()>
    where
        T: AsRef<str> + Display,
        M: AsRef<str> + Display,
    {
        writeln!(&mut self.out, "{} [{}] {}", "e".red(), task, msg)
    }

    /// Info log function, with task display.
    pub fn task_info<T, M>(&mut self, task: T, msg: M) -> Result<()>
    where
        T: AsRef<str> + Display,
        M: AsRef<str> + Display,
    {
        writeln!(&mut self.out, "{} [{}] {}", "i".blue(), task, msg)
    }
}

impl Default for Log {
    fn default() -> Log {
        Log { out: io::stdout() }
    }
}

extern crate printing_press;

use std::{env, path::Path};

use getopts::Options;

use printing_press::error::{Error, ErrorKind, Result};
use printing_press::log::Log;
use printing_press::TaskFile;

const VERSION: &str = include_str!(concat!(env!("OUT_DIR"), "/version"));

fn main() -> Result<()> {
    if cfg!(target_os="windows") {
        colored::control::set_override(false);
    }

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut log = Log::new();

    let mut opts = Options::new();
    opts.optopt("c", "config", "Sets a customs yaml file", "FILE");
    opts.optflag("h", "help", "Print this help menu");
    opts.optflag("v", "version", "Print program version");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            println!("{}", f);

            return Ok(());
        }
    };

    if matches.opt_present("h") {
        log.info(opts.usage(&format!("Usage: {} TASK [options]", program)))?;

        return Ok(());
    }

    if matches.opt_present("v") {
        for line in VERSION.lines() {
            log.info(line)?;
        }

        return Ok(());
    }

    let file = if Path::new(".press").exists() {
        Some(Path::new(".press"))
    } else if Path::new("press.yaml").exists() {
        Some(Path::new("press.yaml"))
    } else if Path::new("press.yml").exists() {
        Some(Path::new("press.yml"))
    } else {
        None
    };

    let task_file = if let Some(path) = file {
        let task_file = TaskFile::load(path)?;

        Ok(task_file)
    } else if matches.opt_present("c") {
        if let Some(ref config) = matches.opt_str("c") {
            let path = Path::new(config);

            if path.exists() {
                let task_file = TaskFile::load(path)?;

                Ok(task_file)
            } else {
                log.error(format!("could not find task file `{}`", path.display()))?;

                Err(Error::from(ErrorKind::TaskFileNotFound))
            }
        } else {
            unreachable!();
        }
    } else {
        log.error(format!(
            "could not find `.press`, `press.yaml`, or `press.yml` in `{}` or any parent directory",
            std::env::current_dir()?.display()
        ))?;

        Err(Error::from(ErrorKind::TaskFileNotFound))
    };

    if !matches.free.is_empty() {
        task_file?.run(&mut log, matches.free)
    } else {
        task_file?.print_tasks(&mut log)
    }
}

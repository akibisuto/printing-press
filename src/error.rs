//! Printing Press error wrapper.
//!
//! A wrapper around errors from the standard library, dependencies, and from `Printing Press` itself.

use std::{error, fmt, io};

use serde_yaml::Error as YamlError;

/// Wrapper around the standard result.
pub type Result<T> = std::result::Result<T, Error>;

/// Error wrapper around the standard library, dependencies, and internal errors.
#[derive(Debug)]
pub struct Error {
    /// The layer the error comes from.
    pub layer: Layer,
    /// If its internal, what kind of error is it.
    pub kind: Option<ErrorKind>,
    /// It is not internal, the original error.
    pub error: Option<Box<dyn error::Error + Send + Sync>>,
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self.layer {
            Layer::Standard | Layer::Dependency => match self.error {
                Some(ref err) => write!(
                    fmt,
                    "[Printing Press] Level: {}, Error: {}",
                    self.layer, err
                ),
                None => write!(fmt, "[Printing Press] Level: {}", self.layer),
            },
            Layer::Internal => write!(
                fmt,
                "[Printing Press] Level: {}, Error: {}",
                self.layer,
                match self.kind {
                    Some(ref kind) => kind.as_str(),
                    None => "No Kind",
                },
            ),
        }
    }
}

impl error::Error for Error {
    fn cause(&self) -> Option<&dyn error::Error> {
        match self.layer {
            Layer::Standard | Layer::Dependency => match self.error {
                Some(ref err) => err.cause(),
                None => None,
            },
            Layer::Internal => None,
        }
    }

    fn description(&self) -> &str {
        match self.layer {
            Layer::Standard | Layer::Dependency => match self.error {
                Some(ref err) => err.description(),
                None => "|No Description For Error|",
            },
            Layer::Internal => match self.kind {
                Some(ref kind) => kind.as_str(),
                None => "No Kind",
            },
        }
    }
}

impl From<ErrorKind> for Error {
    fn from(kind: ErrorKind) -> Error {
        Error {
            layer: Layer::Internal,
            kind: Some(kind),
            error: None,
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error {
            layer: Layer::Standard,
            kind: None,
            error: Some(Box::new(err)),
        }
    }
}

impl From<YamlError> for Error {
    fn from(err: YamlError) -> Error {
        Error {
            layer: Layer::Dependency,
            kind: None,
            error: Some(Box::new(err)),
        }
    }
}

/// The layer the error came from.
#[derive(Debug)]
pub enum Layer {
    /// The error came from the standard library.
    Standard,
    /// The error came from a dependency.
    Dependency,
    /// The error came from within `Printing Press` itself.
    Internal,
}

impl fmt::Display for Layer {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmt,
            "{}",
            match *self {
                Layer::Standard => "Standard",
                Layer::Dependency => "Dependency",
                Layer::Internal => "Internal",
            }
        )
    }
}

/// The kind of the internal error.
#[derive(Debug)]
pub enum ErrorKind {
    /// Task not found (duh)
    TaskNotFound,
    /// No tasks found in the task file
    NoTasks,
    /// No task file
    TaskFileNotFound,
}

impl ErrorKind {
    fn as_str(&self) -> &str {
        match *self {
            ErrorKind::TaskNotFound => "task not found, be sure that all tasks are slept correctly",
            ErrorKind::NoTasks => "no tasks found in the task file",
            ErrorKind::TaskFileNotFound => "no task file found in current path",
        }
    }
}

//! Simple template language based off [Clam](https://github.com/uma0317/clam).

use std::collections::BTreeMap;

#[derive(Debug, PartialEq)]
enum State {
    Template,
    Variable,
    Unknown,
}

/// Very simple template engine.
pub struct Template<'a> {
    data: &'a BTreeMap<String, String>,
    template: &'a str,
}

impl<'a> Template<'a> {
    /// Create a new template with the given string and data map.
    pub fn new(data: &'a BTreeMap<String, String>, template: &'a str) -> Template<'a> {
        Template { data, template }
    }

    /// Render template.
    pub fn render(&self) -> String {
        let mut state = State::Unknown;
        let mut rendered = String::new();
        let mut current_variable = String::new();
        let mut chars = self.template.chars().peekable();

        while let Some(ref c) = chars.next() {
            match c {
                '{' => {
                    if let Some(p) = chars.peek() {
                        if state != State::Variable {
                            if p == &'{' {
                                state = State::Variable;
                            } else {
                                rendered.push(*c);
                            }
                        }
                    }
                }
                '}' => {
                    if state == State::Variable {
                        if let Some(p) = chars.peek() {
                            if p == &'}' {
                                state = State::Variable;
                            } else if let Some(data) = self.data.get(&current_variable) {
                                state = State::Template;
                                rendered.push_str(&data);
                                current_variable = String::new();
                            }
                        } else if let Some(data) = self.data.get(&current_variable) {
                            state = State::Template;
                            rendered.push_str(&data);
                            current_variable = String::new();
                        }
                    } else {
                        rendered.push(*c);
                    }
                }
                _ => match state {
                    State::Unknown | State::Template => {
                        if state == State::Unknown {
                            state = State::Template;
                        }

                        rendered.push(*c);
                    }
                    State::Variable => {
                        if !c.is_whitespace() {
                            current_variable.push(*c);
                        }
                    }
                },
            }
        }

        rendered
    }
}

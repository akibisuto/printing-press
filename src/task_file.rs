//! A task file and its immediate requirements.
//!
//! This is the main entry point to use a `Printing Press` task file.
//!
//! # Example: Using `&str`
//! ```rust,no_run
//! use printing_press::TaskFile;
//!
//! let _ = TaskFile::load("press.yml");
//! ```
//!
//! # Example: Using `Path`
//! ```rust,no_run
//! use std::path::Path;
//! use printing_press::TaskFile;
//!
//! let file = Path::new("press.yml");
//! let _ = TaskFile::load(file);
//! ```

use std::collections::BTreeMap;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use colored::Colorize;
use serde::{Deserialize, Deserializer};

use crate::{
    error::{Error, ErrorKind, Result},
    log::Log,
    task::Task,
};

/// A task file.
#[derive(Debug, Deserialize)]
pub struct TaskFile {
    /// Environmental variables to be past to the commands.
    pub env: Option<BTreeMap<String, String>>,
    /// How logs should be output.
    #[serde(default)]
    pub output: TaskOutput,
    /// Project details.
    pub project: TaskProject,
    /// Map of task for the project.
    pub tasks: Option<BTreeMap<String, Task>>,
    /// Variables to be past to the template manager.
    pub vars: Option<BTreeMap<String, String>>,
    /// The version of the task file 'standard' that should be used.
    pub version: String,
}

impl TaskFile {
    /// Load the task file from the given path.
    ///
    /// # Example: Using `&str`
    /// ```rust,no_run
    /// use printing_press::TaskFile;
    ///
    /// let _ = TaskFile::load("press.yml");
    /// ```
    ///
    /// # Example: Using `Path`
    /// ```rust,no_run
    /// use std::path::Path;
    /// use printing_press::TaskFile;
    ///
    /// let file = Path::new("press.yml");
    /// let _ = TaskFile::load(file);
    /// ```
    pub fn load<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut file = File::open(path)?;
        let mut buffer = String::new();

        let _ = file.read_to_string(&mut buffer)?;

        Self::parse(&buffer)
    }

    /// Load the task file from a `str`.
    ///
    /// This is used internally by `TaskFile::load`.
    ///
    /// # Example
    /// ```rust,no_run
    /// use printing_press::TaskFile;
    ///
    /// const FILE: &str = r#"
    /// version: '1'
    ///
    /// project:
    ///   name: printing press docs
    ///   desc: An example project file
    /// "#;
    ///
    /// let _ = TaskFile::parse(FILE);
    /// ```
    pub fn parse<F: AsRef<str>>(file: F) -> Result<Self> {
        let task_file: TaskFile = serde_yaml::from_str(&file.as_ref())?;

        Ok(task_file)
    }

    /// Run a specific task from global or a project.
    pub fn run(&self, mut log: &mut Log, args: Vec<String>) -> Result<()> {
        let tasks = if let Some(tasks) = &self.tasks {
            tasks
        } else {
            return Err(Error::from(ErrorKind::NoTasks));
        };

        for arg in args {
            let task = if let Some(task) = tasks.get(&arg) {
                task
            } else {
                return Err(Error::from(ErrorKind::TaskNotFound));
            };

            log.info(format!("Running: {}", &arg))?;
            task.run(&self, &mut log, &arg, &self.vars)?;
        }

        Ok(())
    }

    /// Prints all task and their descriptions as a list.
    pub fn print_tasks(&self, log: &mut Log) -> Result<()> {
        log.info("Tasks:")?;

        if let Some(tasks) = &self.tasks {
            for (name, task) in tasks.iter() {
                log.info(format!(
                    "{: >10}  {}",
                    name.bright_yellow(),
                    if let Some(desc) = &task.desc {
                        desc
                    } else {
                        ""
                    }
                ))?;
            }

            Ok(())
        } else {
            log.error("No tasks found in task file")?;

            Err(Error::from(ErrorKind::TaskNotFound))
        }
    }
}

/// Information about the project/task file.
#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum TaskProject {
    /// Only project information given is its name.
    Name(String),
    /// Project information with description.
    NameDesc {
        /// The name of the project/task file.
        name: String,
        /// the description of the project/task file.
        desc: String,
    },
}

/// How the output of logs should be formatted.
#[derive(Debug)]
pub enum TaskOutput {
    /// Outputted logs will be prefixed with `[<task name>] <log>`.
    Task,
    /// Outputted logs will be prefixed with `[<custom prefix>] <log>`.
    Prefix,
}

impl Default for TaskOutput {
    fn default() -> TaskOutput {
        TaskOutput::Task
    }
}

impl<'de> Deserialize<'de> for TaskOutput {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        Ok(match s.to_lowercase().as_str() {
            "task" => TaskOutput::Task,
            "prefix" => TaskOutput::Prefix,
            _ => TaskOutput::Task,
        })
    }
}

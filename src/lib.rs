//! A Yaml task runner written in Rust
//!
//! This is the back-end of `Printing Press` it handles the parsing and running of tasks and task files.
//!
//! # Example: Using `&str`
//! ```rust,no_run
//! use printing_press::TaskFile;
//!
//! let _ = TaskFile::load("press.yml");
//! ```
//!
//! # Example: Using `Path`
//! ```rust,no_run
//! use std::path::Path;
//! use printing_press::TaskFile;
//!
//! let file = Path::new("press.yml");
//! let _ = TaskFile::load(file);
//! ```

#![deny(
    missing_docs,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unstable_features,
    unused_import_braces,
    unused_qualifications,
    unused_results
)]

#[macro_use]
extern crate serde_derive;

pub mod error;
pub mod log;
pub mod task;
pub mod task_file;
pub mod template;

pub use crate::task_file::TaskFile;

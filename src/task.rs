//! A task and its immediate requirements.
//!
//! This handles running a task, its dependencies, and its hooks.

use std::collections::{BTreeMap, BTreeSet};
use std::io::{BufRead, BufReader};
use std::process::{Command as StdCommand, Stdio};

use serde::{Deserialize, Deserializer};

use crate::{
    error::{Error, ErrorKind},
    log::Log,
    task_file::TaskFile,
    template::Template,
};

/// A task of a task file.
#[derive(Debug, Deserialize)]
pub struct Task {
    /// Task to be ran before this task.
    pub deps: Option<BTreeSet<String>>,
    /// The description of this task.
    pub desc: Option<String>,
    /// The output location that this task generates. (Used for runtime update checking)
    pub dist: Option<String>,
    /// The working directory this should be ran in.
    pub dir: Option<String>,
    /// The command(s) to be executed.
    pub exec: Option<TaskExec>,
    /// The method that should be used for runtime checking.
    #[serde(default)]
    pub method: TaskMethod,
    /// If it should hide any logging done by the command(s).
    #[serde(default)]
    pub silent: bool,
    /// The input location that this task generates. (Used for runtime update checking)
    pub src: Option<String>,
    /// Variables to be past to the template manager.
    pub vars: Option<BTreeMap<String, String>>,
}

impl Task {
    /// Run the task with the given variables from the parent task file.
    pub fn run(
        &self,
        file: &TaskFile,
        mut log: &mut Log,
        name: &str,
        parent_vars: &Option<BTreeMap<String, String>>,
    ) -> Result<(), Error> {
        let mut all_vars: BTreeMap<String, String> = BTreeMap::new();

        if let Some(ref vars) = parent_vars {
            all_vars.extend(vars.iter().map(|(k, v)| (k.clone(), v.clone())));
        }

        if let Some(ref vars) = &self.vars {
            all_vars.extend(vars.iter().map(|(k, v)| (k.clone(), v.clone())));
        }

        if let Some(ref deps) = &self.deps {
            if let Some(ref tasks) = file.tasks {
                for dep in deps {
                    match tasks.get(dep) {
                        Some(task) => {
                            task.run(&file, &mut log, name, &parent_vars)?;
                        }
                        None => {
                            log.error(format!("Task: {}, not found", dep))?;

                            return Err(Error::from(ErrorKind::TaskNotFound));
                        }
                    }
                }
            }
        }

        if let Some(ref exec) = self.exec {
            match exec {
                TaskExec::Single(ref cmd) => {
                    self.exec(name, &mut log, &all_vars, cmd.cmd())?;
                }
                TaskExec::List(ref cmds) => {
                    for cmd in cmds {
                        self.exec(name, &mut log, &all_vars, cmd.cmd())?;
                    }
                }
            }
        }

        Ok(())
    }

    fn exec(
        &self,
        name: &str,
        log: &mut Log,
        vars: &BTreeMap<String, String>,
        cmd: &str,
    ) -> Result<(), Error> {
        let rendered = Template::new(vars, cmd).render();

        let mut process = StdCommand::new(if cfg!(target_os = "windows") {
            "cmd"
        } else {
            "sh"
        })
        .args(&[
            if cfg!(target_os = "windows") {
                "/C"
            } else {
                "-c"
            },
            &rendered,
        ])
        .stderr(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

        if let Some(ref mut stdout) = process.stdout {
            if !self.silent {
                for line in BufReader::new(stdout).lines() {
                    let line = line?;
                    log.task_info(name, line)?;
                }
            }
        }

        if let Some(ref mut stderr) = process.stderr {
            for line in BufReader::new(stderr).lines() {
                let line = line?;
                log.task_error(name, line)?;
            }
        }

        let status = process.wait()?;
        log.task_info(name, format!("Finished with status {:?}", status))?;

        Ok(())
    }
}

/// A wrapper over the types of commands that can be using in the `exec` field.
#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum TaskExec {
    /// A list of string commands.
    Single(Command),
    /// A list of detailed commands.
    List(Vec<Command>),
}

/// A wrapper over the different types of commands so they can be changed half way though.
#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum Command {
    /// A single string command.
    String(String),
    /// A command or task runner.
    Exec(Exec),
}

impl Command {
    /// Get the command string.
    pub fn cmd(&self) -> &str {
        match self {
            Command::String(ref cmd) => cmd,
            Command::Exec(ref exec) => match exec.cmd {
                Runner::Command(ref cmd) => cmd,
            },
        }
        .as_str()
    }
}

/// A task command that has detailed commands.
#[derive(Debug, Deserialize)]
pub struct Exec {
    /// If error should be ignored.
    pub ignore_error: Option<bool>,
    /// A string command.
    #[serde(flatten)]
    pub cmd: Runner,
    /// If it should hide any logging done by the command(s).
    pub silent: Option<bool>,
    /// Variables to be past to the template manager.
    pub vars: Option<BTreeMap<String, String>>,
}

/// A runner is either a command or task string.
#[derive(Debug, Deserialize)]
pub enum Runner {
    /// A command string.
    #[serde(rename = "cmd")]
    Command(String),
    // /// A task string.
    // #[serde(rename = "task")]
    // Task(String),
}

/// Different systems for runtime checking.
#[derive(Debug)]
pub enum TaskMethod {
    /// File will be compared by their modified times.
    Time,
    /// File will be compared by their checksum.
    Checksum,
}

impl Default for TaskMethod {
    fn default() -> TaskMethod {
        TaskMethod::Time
    }
}

impl<'de> Deserialize<'de> for TaskMethod {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        Ok(match s.to_lowercase().as_str() {
            "time" => TaskMethod::Time,
            "checksum" => TaskMethod::Checksum,
            _ => TaskMethod::Time,
        })
    }
}

# Printing Press

Printing Press is a Yaml file task runner written in Rust based off [Task](https://github.com/go-task/task).

## Examples

```yaml
version: '1' # printing Press version

project: # can also just be 'coven'
  name: coven
  desc: CSS framework for Akibisuto projects

output: task # can be either 'task' or 'prefix'

vars: # variables accessible throughout the file
  name: coven # <key>: <value>

tasks:
  build:
    desc: Build the crate in release mode
    exec: cargo build --release # exec can be a single command
    deps: # or exclude exec and have a list of dependencies
      - build-server
      - build-client
    silent: true # no output will be given unless theres an error
  build-server:
    desc: Install the crate to the global .cargo folder
    exec: # or a list
      - cp client/client.js static/app.js
      - tsc
  build-client:
    desc: print some text
    exec: echo {{name}} v{{version}} # they also use a handlebars like templates
    vars: # local variables
      version: "1.0.1"
```
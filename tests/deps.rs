extern crate printing_press;

use printing_press::{error::Error, log::Log, TaskFile};

const PASS: &str = r#"
version: '1'

project:
  name: printing-press-tests
  desc: Tests for printing-press

output: task

tasks:
  run:
    deps:
      - dep1
      - dep2
    exec: echo run
  dep1:
    exec: echo dep1
  dep2:
    exec: echo dep2
"#;

#[test]
fn pass() -> Result<(), Error> {
    let mut log = Log::new();
    let task_file = TaskFile::parse(&PASS)?;

    task_file.run(&mut log, vec!["run".to_string()])?;

    Ok(())
}

const FAIL: &str = r#"
version: '1'

project:
  name: printing-press-tests
  desc: Tests for printing-press

output: task

tasks:
  run:
    deps:
      - dep-1
      - dep2
    exec: echo run
  dep1:
    exec: echo dep1
  dep2:
    exec: echo dep2
"#;

#[test]
#[should_panic]
fn fail() {
    let mut log = Log::new();
    let task_file = TaskFile::parse(&FAIL).unwrap();

    task_file.run(&mut log, vec!["run".to_string()]).unwrap();
}
